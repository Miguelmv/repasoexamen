<?= $this->extend('layout/plantilla') ?>

<?= $this->section('content') ?>
<?php $session = \Config\Services::session(); ?>

<?php
    $auth = new \IonAuth\Libraries\IonAuth();
?>

<?php if ($auth->loggedIn() AND $auth->isAdmin()OR $auth->inGroup('secretaria')): ?>
<?php $user = $auth->user()->row(); ?>
<a href="<?= site_url('pauController/afegir') ?>" class="btn btn-primary">Afegir</a>


<div class="d-flex flex-row-reverse bd-highlight">
   <?= $user->first_name . ' ' . $user->last_name ?>
</div>

<div class="d-flex flex-row-reverse bd-highlight"><span>
       
        
            <a href="<?= site_url('auth/logout'); ?>">Desconectar</a>
         <?php else: ?>
          
        </span>
    
</div>
    <div class="p-2 bd-highlight"><span>
            <a  href="<?= site_url('auth/login') ?>">Entrar</a>
        <?php endif; ?>
        </span></div>
    <div class="p-2 bd-highlight"><a href="<?=site_url('carroController/carro'); ?>"><?=$session->has('carro') ? count($session->carro) : 'esta vacio'?></a>
        
        </div>

<table class="table table-striped table-condensed" id="myTable">
    <thead>
        <?php if ($auth->loggedIn() AND $auth->isAdmin()OR $auth->inGroup('secretaria')): ?>
        <th>NIE/NIF</th>
        <?php endif; ?>
        <th>Solicitante</th>
        <?php if ($auth->loggedIn() AND $auth->isAdmin()OR $auth->inGroup('secretaria')): ?>
        <th>email</th>
        <?php endif; ?>
        <th>ciclo</th>
        <th>matrícula</th>
        <th></th>
    </thead>
    <?php foreach ($solicitudes as $solicitud): ?>
        <tr>
            <?php if ($auth->loggedIn() AND $auth->isAdmin()OR $auth->inGroup('secretaria')): ?>
            <td><?= $solicitud['nif'] ?></td>
            <?php endif; ?>
            <td><?= $solicitud['solicitante'] ?></td>
            <?php if ($auth->loggedIn() AND $auth->isAdmin()OR $auth->inGroup('secretaria')): ?>
            <td><?= $solicitud['email'] ?></td>
            <?php endif; ?>
            <td><?= $solicitud['nombre'] ?></td>
            <td>
                <?= $solicitud['tipo_tasa']==1 ? 'ordinaria' : ($solicitud['tipo_tasa']==3 ? 'gratuita' : 'semigratuita') ?>
            </td>
            <td><a href="<?= site_url('carroController/comprar/'.$solicitud['id']); ?>" title="Añadir al carrito de la compra"" class="btn btn-info"> Cistella
                </a>
                <?php if ($auth->loggedIn() AND $auth->isAdmin()): ?>
                <a href="<?= site_url('pauController/borrar/'.$solicitud['id'])?>" 
                   class="btn btn-danger btn-sm" onclick="return confirm('Estás seguro de borrar la solicitud de <?= $solicitud['solicitante'] ?>')">Borrar</a>
            <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>

<?= $this->endSection() ?>
