<?= $this->extend('layout/plantilla') ?>
<?= $this->section('content') ?>
<?php $session = \Config\Services::session(); ?>
<?php if ($session->has('carro')): ?>
    <?php $carro = $session->get('carro'); ?>
    <div class="row"> 
        <table class="table table-striped">
            <thead>
            <th>NIE/NIF</th>
            <th>email</th>
            <th>Nombre Alumno</th>
            <th>matrícula</th>
            </thead>
            <?php foreach ($carro as $solicitud): ?>
                <tr>
            <td><?= $solicitud['nif'] ?></td>
            <td><?= $solicitud['email'] ?></td>
            <td><?= $solicitud['nombre'] ?></td>
            <td>
                <?= $solicitud['tipo_tasa']==1 ? 'ordinaria' : ($solicitud['tipo_tasa']==3 ? 'gratuita' : 'semigratuita') ?>
            </td>
                </tr>
            <?php endforeach; ?> 
        </table>
        <a href="<?= site_url('carroController/borrarCarro') ?>" class="btn btn-danger">Vacía carro</a>
        <a href="<?= site_url('/') ?>" class="btn btn-secondary">Seguir Comprando</a>
    </div>                              
<?php else : ?>
    <h3>No hay artículos</h3>
    <p>El carro está vacio</p>
<?php endif ?>

<?= $this->endSection() ?> 