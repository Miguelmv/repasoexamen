<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Validation;

/**
 * Description of CustomRules
 *
 * @author jose
 */
class CustomRules {
    //Regla per validar el DNI o el NIE
    public function nifValidation(string $str){
        if (preg_match('/^[XYZ0-9][0-9]{7}[A-Z]/', $str)) {
            for ($i = 0; $i < 9; $i ++){
              $num[$i] = substr($str, $i, 1);
            }

            if ($num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKE', substr(str_replace(['X','Y','Z'], ['0','1','2'], $str), 0, 8) % 23, 1)) {
              return true;
            } else {
              return false;
            }
      }
    }
}
