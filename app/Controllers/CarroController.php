<?php

namespace App\Controllers;
use Config\Services;
use App\Models\PauModel;
use App\Models\CiclosModel;

class CarroController extends BaseController{
    
     protected $session;
     protected $datos;
     
    public function comprar ($id){
        //Comprobar si el artículo está en el carro
       if ($this->session->has('carro')) {
           //existe el carro
           $carro = $this->session->get('carro');
           if (!isset($carro[$id])){
            //no existe el elmento
            $solicitud = $this->datos->find($id);
            $solicitud['id']=1;
            $carro[$id]= $solicitud; //añadimos un elemento al array
           }
       } else {
           //crear el carro
           $solicitud = $this->datos->find($id);
           $carro[$id] = $solicitud;
           $solicitud['id']=1;
       }
       //var_dump($this->session->carro);
       //y en todos los casos guardar la variable de sesion
       $this->session->set('carro',$carro);
       return redirect()->to(site_url('/'));
    }
    
   
  
    
    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        //--------------------------------------------------------------------
        // Preload any models, libraries, etc, here.
        //--------------------------------------------------------------------
        $this->datos = new PauModel();
        $this->session = Services::session();
    } 

    
    public function carro(){
        //definimos como queremos paginar
        $data = [
            'titol' => "Carro de la compra de Miguel",
        ];
        //Ahora los datos no hemos de buscarlos en la base de datos, 
        // están en la variable de sesión.
        echo view('solicitudes/carroview', $data);
    }

    public function borrarCarro(){
        $this->session->remove('carro');
        return redirect()->to(site_url('carroController/carro'));
    } 
}
